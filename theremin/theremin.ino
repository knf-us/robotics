int buzzer_pin = 5;
int photoresistor_pin = A0;
 
void setup()
{
  Serial.begin(9600);
  pinMode(photoresistor_pin, INPUT);
  pinMode(buzzer_pin, OUTPUT);
}
 
void loop()
{
  int val = analogRead(photoresistor_pin) / 4 + 600;
  Serial.println(val);
  tone(buzzer_pin, val);
  delay(30);
}
