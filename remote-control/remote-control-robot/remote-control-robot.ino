#include <DiffDrive.h>

#include <SoftwareSerial.h>
#include <SerialCommand.h>

SerialCommand SCmd;

DiffDrive Motors(3, 12, 13, 5, 6, 7);

void setup()
{
  Serial.begin(9600);
  SCmd.addCommand("x", parseCmd);
}

void loop()
{
  SCmd.readSerial();
}

void parseCmd()    
{
  int val[3];  
  char *arg; 
  
  for(int i=0; i<3; i++)
  {
    arg = SCmd.next(); 
    if (arg != NULL) val[i] = atoi(arg);
    else return;
  }
  
  Motors.Drive(val[0], val[1], val[2]);
}
    
  


