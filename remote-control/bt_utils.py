#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bluetooth as bt


def list_devices():
	
	devices = bt.discover_devices(lookup_names=True)
	for entry in devices:
		print '%s: %s' % entry
		
	return devices
	
			
def get_device(dev_name):
	
	try:
	    return [dev for (dev, name) in list_devices() if name == dev_name][0]
	except IndexError:
	    return None
		
		
if __name__ == "__main__":
	list_devices()
	
