#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bluetooth as bt
import sys
import pygame
from pygame.locals import *
from math import fabs, ceil, copysign


class RobCon:


    def __init__(self):
        # global PAD_RELOAD

        self.fps = 15

        pygame.init()
        pygame.display.set_caption("Robot Control App")
        self.surf = pygame.display.set_mode((640, 480))

        self.font = pygame.font.Font('freesansbold.ttf', 40)

        self.clock = pygame.time.Clock()

        self.dev_name = 'HC-05'
        self.bt_connect()

        self.pad_mode = False
        self.change_mode()

        # PAD_RELOAD = USEREVENT+1
        # pygame.time.set_timer(PAD_RELOAD, 1000)

    def run(self):
        while True:
            self.loop()
            self.update()

    def loop(self):

        for event in pygame.event.get():
            if event.type == QUIT:
                self.terminate()
            elif event.type == KEYDOWN:
                if event.key == K_m and pygame.key.get_mods() & KMOD_CTRL:
                    self.change_mode()
            # elif event.type == PAD_RELOAD:
                # self.pad_mode = self.pad_setup()
                # self.clock.tick(20)

        self.get_xy()

    def update(self):

        # A tu jest totalna chała z wyglądem aplikacji, ale to mi się nie chce.
        self.surf.fill((0, 0, 0))

        scoreSurf = self.font.render('(%s,%s)' % (self.dir1, self.dir2),
                                     True, (255, 255, 255))
        scoreRect = scoreSurf.get_rect()
        scoreRect.topleft = (640 - 360, 240)
        self.surf.blit(scoreSurf, scoreRect)

        pygame.display.update()
        # Koniec chały

        self.send()

        self.clock.tick(self.fps)

    def bt_connect(self):

        # Trzeba ogarnąć ten wybor portu
        port = 1
        # Czy to jest przejrzyste, czy można napisać to ładniej?
        adress = next(iter((dev for (dev, name)
                            in bt.discover_devices(lookup_names=True)
                            if name == self.dev_name)
				          ), None)

        self.sock = bt.BluetoothSocket(bt.RFCOMM)

        try:
            self.sock.connect((adress, port))
        except TypeError:
            print u"Couldn't find device with {0:s} name.".format(self.dev_name)
            self.sock = None
        except bt.BluetoothError:
            print u"Couldn't connect with {0:s} device.".format(self.dev_name)
            self.sock = None

        if self.sock is None:
            self.terminate()

    def send(self):

        try:
            self.sock.send('x&{0}&{1}&{2}\r'.format(self.vel, self.dir1,
                                                    self.dir2))
        except bt.BluetoothError:
            print "Connection lost!"
            print 'Terminate in 3, 2, 1...'
            self.terminate()

    def get_xy(self):

        if self.pad_mode:
            #xl, yl = self.pad.get_axis(0), -self.pad.get_axis(1)
            #xr, yr = self.pad.get_axis(3), -self.pad.get_axis(2)
            yl, xr = -self.pad.get_axis(1), self.pad.get_axis(3)

            self.vel = int(ceil(255 * fabs(yl)))
            if xr <> 0:
                self.dir1 = copysign(1, yl * xr)
                self.dir2 = -self.dir1
                self.vel = int(0.75 * self.vel)
            else:
                self.dir1 = copysign(1, yl)
                self.dir2 = self.dir1
        else:
            kstate = pygame.key.get_pressed()
            x = kstate[K_RIGHT] - kstate[K_LEFT]
            y = kstate[K_UP] - kstate[K_DOWN]

            self.vel = 255 * abs(y)
            if x <> 0:
                self.dir1 = copysign(1, y * x)
                self.dir2 = -self.dir1
                self.vel = int(0.75 * self.vel)
            else:
                self.dir1 = copysign(1, y)
                self.dir2 = self.dir1

        self.dir1, self.dir2, = int(self.dir1), int(self.dir2)

    def change_mode(self):

        if self.pad_mode:
            self.pad = None
            self.pad_mode = False
        else:
            self.pad_mode = self.pad_setup()

    def pad_setup(self):

        pygame.joystick.quit()
        pygame.joystick.init()

        pad_count = pygame.joystick.get_count()
        if pad_count > 0:
            self.pad = pygame.joystick.Joystick(0)
            self.pad.init()
            return True

        return False

    def pad_reload(self):

        state = self.pad_mode
        if self.pad_mode:
            self.change_mode()
            self.change_mode()
            print 'Pad unplugged. Plug pad back to use pad mode.'
        else:
            self.change_mode()
            if state != self.pad_mode:
                print 'Press CTRL+M to activate pad mode.'

    def terminate(self):
        pygame.quit()
        if self.sock is not None:
            try:
                self.sock.send('x&{0}&{1}&{2}\r'.format(0, 1, 1))
            except bt.BluetoothError:
                pass
            self.sock.close()
        sys.exit()


def main():
    rc = RobCon()
    rc.run()

if __name__ == '__main__':
    main()
