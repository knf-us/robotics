#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bluetooth as bt
import Tkinter as tk


def connect_bt():
		
	global sock, adress
		
	try:
		sock = bt.BluetoothSocket(bt.RFCOMM)
		sock.connect((adress, 1))
	except bt.BluetoothError:
		pass
		
		
def send_bt():
		
	global sock, text_box
	
	try:
		sock.send("x&"+text_box.get()+"\r")
	except bt.BluetoothError:
		pass
		
		
def reset_bt():
		
	global sock
	
	try:
		sock.send("x&Witaj przechodniu! Wpisz tekst.\r")
	except bt.BluetoothError:
		pass


def main():
	
	global sock, adress, text_box
	
	root = tk.Tk()
	root.wm_title("Simple BT App")
	root.minsize(230, 30)
	# Code to add widgets will go here...
	
	text_box = tk.Entry(root, font = "Arial 14", bd=2)
	text_box.pack(side = tk.TOP)
	
	connect = tk.Button(root, text="Connect", bd=2, command=connect_bt)
	connect.pack(side = tk.LEFT)
	
	reset = tk.Button(root, text="Reset", bd=2, command=reset_bt)
	reset.pack(side = tk.LEFT)
	
	send = tk.Button(root, text="Send", bd=2, command=send_bt)
	send.pack(side = tk.LEFT)
	
	root.mainloop()
		

if __name__ == '__main__':
	
	sock = bt.BluetoothSocket(bt.RFCOMM)
	adress = '20:14:10:10:14:06'
	
	main()
